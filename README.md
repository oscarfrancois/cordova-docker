# Procédure pour créer votre premier HelloWorld natif sur un Smartphone

![hello world](res/hello_world.jpg)

Ce tutoriel est utilisable sous licence "Attribution-NonCommercial-ShareAlike 4.0 International".

![android](res/license_cc.jpg)


# Introduction

Nous utiliserons les technologies suivantes durant cette activité:

![android](res/android.jpg)
![apache_cordova](res/apache_cordova.jpg)
![apache_cordova](res/css.jpg)
![apache_cordova](res/docker.jpg)
![apache_cordova](res/html5.jpg)
![apache_cordova](res/js.jpg)
![apache_cordova](res/linux.jpg)

Le procédure suivante est prévue pour Windows 10. Les principes sont identiques pour un environnement natif Linux.

# Pré-requis

Vous devez effectuer les étapes d'installation suivantes avant de pouvoir générer votre premier HelloWorld.

## Android Platform Tools

<img src="res/android.jpg" width="80">

Android Platform Tools permet de déployer des exécutables sur des smartphones à base de système d'exploitation Android/linux.

Voici le lien pour télécharger cet outil: https://developer.android.com/studio/releases/platform-tools

Faites l'extraction de cette archive.

Ajoutez le répertoire contenant l'exécutable adb.exe dans la variable d'environnement Path de Windows.

Cette variable d'environnement Path est accessible via:

Panneau de configuration > Système et sécurité > Système > Paramètres avancés du système > Variables d'environnement

Pour vérifier cette étape, lancez un powershell et tapez:

` adb --version`

vous devriez voir la version actuelle de l'outil. Sinon un message d'erreur s'affichera.

## Windows Subsystem for Linux, WSL2

Téléchargez la mise à jour du noyau Linux WSL2 pour machine X64 via le lien suivant:

https://docs.microsoft.com/en-us/windows/wsl/install-manual#step-4---download-the-linux-kernel-update-package

Sans cette mise à jour, vous ne pourrez pas générer d'exécutable pour smartphone.

## Docker desktop

<img src="res/docker.jpg" width="150">

Docker permet d'installer des systèmes d'exploitation (Operating System, OS) légers, généralement dérivés de linux, pré-configurés avec des suites logicielles (par exemple un serveur web et sa base de données).

Un mécanisme particulièrement léger et rapide dénommé conteneur ("container") est utilisé pour faire fonctionner ces OS. Ces OS fonctionnent à l'intérieur d'un OS principal (mac, windows ou linux).

Voici le lien pour télécharger cet outil: https://www.docker.com/products/docker-desktop/

Durant l'installation, choisissez le mécanisme de virtualisation Windows Subsystem for Linux (WSL) quand on vous pose la question.

Acceptez la fermeture de session windows qui vous sera peut-être proposé pour finaliser l'installation. Sans cette étape, cet outil ne fonctionnera pas.

L'ordinateur doit aussi avoir la virtualization activée au niveau du BIOS (Basic Input Output Operating System). En cas de message d'erreur au lancement de Docker desktop, allez dans le BIOS de l'ordinateur au tout début du démarrage (généralement via la combinaison de touches escape "Esc" puis "Del" ou "Suppr" ou "F1"), cherchez le menu concernant la virtualisation (en général dans les menus du processeur), activez la et sauvegardez vos changements.

# Initialisation de votre conteneur docker cordova

## Récupération de l'image docker cordova

Ouvrez un powershell

<img src="res/powershell.jpg" width="50">

Puis récupérez l'image docker cordova

`docker pull beevelop/cordova`

## Création du conteneur cordova

Dans un powershell, exécutez les commandes suivantes:

`cd $env:USERPROFILE`

`docker run -dit --name cordova-ct --mount type=bind,source="$PWD",target=/tmp/share -w /tmp/share beevelop/cordova bash`

Explications:

La commande `docker run` ci-dessus démarre un conteneur en tâche de fond. Celui-ci est basé sur une image cordova. Il fait correspondre le répertoire courant de l'utilisateur windows au répertoire `/tmp/share` dans le conteneur.

Ainsi vous pourrez coder depuis windows avec l'éditeur de votre choix et seulement générer l'exécutable android dans le conteneur.

Remarque: `$PWD` est la variable d'environnement windows contenant le répertoire courant du powershell.

# Création de votre premier projet cordova

`docker exec cordova-ct cordova create myapp myapp.company.name MyApp`

`docker exec -w /tmp/share/myapp cordova-ct cordova platform add android`

`docker exec -w /tmp/share/myapp cordova-ct cordova run android`

<img src="res/clap.jpg" width="50">

Bravo, votre premier HelloWorld vient d'être généré! il ne reste plus qu'à le déployer sur votre smartphone.

# Déploiement sur un smartphone

<img src="res/smartphone.jpg" width="150">

Ce tutoriel n'adresse que les smartphones Android. Mais la solution Cordova permet de déployer le même code source aussi pour un iPhone.

Quelques actions sont nécessaires sur le smartphone avant de pouvoir déployer votre HelloWorld.
Ces actions ne sont à effectuer que la première fois.

## Activation d'Android Debug Bridge, ADB

Un menu caché d'Android permet d'activer un mode pour les développeurs. Ce mode est nécessaire pour installer votre premier HelloWorld.

Allez dans les paramètres ("Settings") puis "A propos de" ("About phone"), et appuyez à de multiples reprises sur le menu "Build number" jusqu'à ce qu'un message vous informe que le mode développeur est activé.

Allez ensuite dans: Paramètres ("Settings") > Système ("System") > Avancé ("Advanced") > 
Options développeur ("Developer options").

Activez l'option débogage Android ("Android debugging").

Vérifiez que l'option "Rester actif" ("Stay awake") est bien activée.

Dans Paramètres ("Settings") > Ecran ("Display") > Avancé ("Advanced") > Mise en veille ("Screen timeout"), fixez un délai de 30 minutes.

Branchez le smartphone à votre ordinateur.

Remarque: durant le premier déploiement de votre HelloWorld, un message apparaitra sur votre smartphone ("Unauthorized..."). Pensez à cocher la case et accepter sinon le déploiement ne se fera pas. Ce message n'apparaitra qu'au premier déploiement.

## Retour à windows pour le déploiement sur le smartphone de votre premier hello world

Ouvrez un second powershell.

Les commandes suivantes récupèrent l'exécutable android généré dans le conteneur puis l'installent sur le smartphone et démarrent l'application.

`adb install -r "$env:USERPROFILE\myapp\platforms\android\app\build\outputs\apk\debug\app-debug.apk`

`adb shell am start -n myapp.company.name/myapp.company.name.MainActivity`

Si tout s'est bien passé, vous devriez voir apparaitre sur votre smartphone votre première application smartphone dont l'aspect sera le suivant:

![android](res/hello_world_app.jpg)


## Automatiser la compilation et le déploiement de votre application

Les étapes de compilation, déploiement puis lancement d'une application mobile étant très fréquentes, il est opportun de les automatiser.

Voici ci-dessous un script powershell qui peut être couplé à votre éditeur de code (par exemple à chaque fois que vous sauvegardez un fichier).

```
$containerName="cordova-ct"
$pathDocker="/tmp/share/myapp/"
$pathLocal="$env:USERPROFILE\myapp\platforms\android\app\build\outputs\apk\debug\"
$Host.UI.RawUI.ForegroundColor="green"
echo ">>>>>> Build Android Code ... `n"
$Host.UI.RawUI.ForegroundColor="white"
docker exec --workdir $pathDocker $containerName cordova run android
$Host.UI.RawUI.ForegroundColor="green"
echo ">>>>>> Install on Android Device ... `n"
$Host.UI.RawUI.ForegroundColor="white"
cd $pathLocal
adb install -r app-debug.apk
$Host.UI.RawUI.ForegroundColor="green"
echo "`n>>>>>> END `n"
$Host.UI.RawUI.ForegroundColor="white"
adb shell am start -n myapp.company.name/myapp.company.name.MainActivity
```


# Pour aller plus loin

## Modification du message d'accueil de votre HelloWorld

Maintenant que vous avez créé votre premier HelloWorld, tentez de faire une modification simple, par exemple juste le changement du message: "APACHE CORDOVA", puis compilez et déployez de nouveau sur votre smartphone pour voir le changement.

Le message "APACHE CORDOVA" est à modifier dans le fichier `www/index.html` du répertoire de votre HelloWorld.

## Vibration du smartphone

Vous pouvez ensuite essayer un des plug-in Cordova pour accéder aux fonctions spécifiques d'un smartphone (exemple: vibration, bandeau de notification, localisation, etc).

Par exemple pour la vibration:

https://cordova.apache.org/docs/en/11.x/reference/cordova-plugin-vibration/

